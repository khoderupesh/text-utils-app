// import './App.css';
// import About from './components/about';
import Alert from './components/alert';
import Navbar from './components/navbar';
import TextConverter from './components/text-converter';
import TimeDateConverter from './components/time-date-converter';
import DataDisplay from "./components/data-display";
import React, { useState } from 'react';
import data from './data/customer-data.json';
import { topicData } from './data/tab-data';
import TabButton from './components/tab-button';

function App() {
  const [selectedTopic, setSelectedTopic] = useState('default');
  function handleClick(selectedButton) {
    console.log(selectedButton)
    setSelectedTopic(selectedButton);
  }
  const [mode, setMode] = useState('light');
  const toggleMode = () =>  {
    if(mode === 'light') {
      setMode('dark');
      showAlert('Set to Dark Mode', 'success')
    } else {
      setMode('light');
      showAlert('Set to Light Mode', 'success')
    }
    setTimeout(() => {
      showAlert()
    }, 1500)
  }
  const [alert, setAlert] = useState(null)
  const showAlert = (message, type) => {
    setAlert({message, type})
  }
  return (
    <>
    <Navbar title="TextUtils" mode={mode} toggleMode={toggleMode} />
    <Alert alert={alert}/>
    <div className="container">
      <TextConverter heading="Text Casing Converter App" />
      {/* <About/> */}
      <TimeDateConverter/>
      {data.map((d) => ( <DataDisplay {...d}/>))}
      {/* <DataDisplay 
        customerId={data[0].customerId} 
        customerName={data[0].customerName} 
        customerAdd={data[0].customerAddress}
      />
      <DataDisplay 
        customerId={data[1].customerId} 
        customerName={data[1].customerName} 
        customerAdd={data[1].customerAddress}
      />
      <DataDisplay 
        customerId={data[2].customerId} 
        customerName={data[2].customerName} 
        customerAdd={data[2].customerAddress}
      /> */}
      <section id="examples">
        <h2>Examples</h2>
        <div className='tab-button-menu'>
          <TabButton title="Components" onSelect={() => handleClick('components')}/>
          <TabButton title="Essentials" onSelect={() => handleClick('Essentials')}/>
          <TabButton title="Library" onSelect={() => handleClick('Library')}/>
        </div>
        <div>
          {selectedTopic ? <h3>{topicData[selectedTopic].title}</h3> : null}
           
        </div>
        <p></p>
      </section>
    </div>
    </>
  );
}

export default App;
