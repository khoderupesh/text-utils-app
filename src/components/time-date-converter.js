export default function TimeDateConverter() {
    return(
        <>
        <form className="row g-3 my-3">
            <h4 className="my-3">Convert epoch to human-readable date and vice versa</h4>
            <div className="col-auto">
                <label htmlFor="text" className="visually-hidden">Password</label>
                <input type="text" className="form-control" id="timestamp"/>
            </div>
            <div className="col-auto">
                <button type="submit" className="btn btn-primary mb-3">Timestamp to Human Date</button>
            </div>
            <div className="col-auto" id="resultBox"></div>
        </form>
        </>
    )
}