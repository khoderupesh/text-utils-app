import React from 'react'
//import data from "../data/customer-data.json";

export default function DataDisplay({customerName, customerAdd, customerId}) {
  return (
    <>
      <div className='my-3'>
          <h3>{customerId}</h3>
        <p id='data-tag'>
          {customerName} <br/>
          {customerAdd}
        </p>
      </div>
    </>
  )
}
