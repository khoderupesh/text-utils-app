import React from 'react'

export default function TabButton(props) {
  // function handleClick() {
  //   console.log('Hello ' + props.title)
  // }
  return (
      <button id="tab-button" className="btn btn-primary m-2" onClick={props.onSelect}>{props.title}</button>
  )
}
